import { useState, useEffect } from "react";
import List from "./components/List/List";
import basketIcon from "./components/assets/shopping-basket.svg";
import basicFavorite from "./components/assets/favorite-false.svg";
import "../src/App.scss";
import "../reset.scss";

function App() {
  const [data, setData] = useState([]);
  const [basketProducts, setBasketProducts] = useState([]);
  const [favProducts, setFavProducts] = useState([]);

  useEffect(() => {
    const fetchData = async () => {
      const response = await fetch(`/products.json`);
      const info = await response.json();
      setData(info);
    };

    fetchData();
  }, []);


  const getDataFromLS = (key) => {
    const lsData= localStorage.getItem(key);
    if (!lsData) return [];
    try {
      const value = JSON.parse(lsData);
      return value;
    } catch (e) {
      return [];
    }
  };


  
  useEffect(() => {
    setBasketProducts(getDataFromLS("Basket"));
    setFavProducts(getDataFromLS("Favorite"));
  }, []);

  useEffect(() => {
    localStorage.setItem("Basket", JSON.stringify(basketProducts));
    localStorage.setItem("Favorite", JSON.stringify(favProducts));
  }, [basketProducts, favProducts]);

  return (
    <>
      <header className="header">
        <img src={basketIcon} className="header__photo" alt="basket" />
        <p className="header__text">{basketProducts.length}</p>
        <img src={basicFavorite} className="header__photo" alt="favorite" />
        <p className="header__text">{favProducts.length}</p>
      </header>
      <List
        setBasketProducts={setBasketProducts}
        setFavProducts={setFavProducts}
        favProducts={favProducts}
        data={data}
      />
    </>
  );
}

export default App;
