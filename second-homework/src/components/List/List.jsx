import Card from "../Card/Card";
import "./List.scss";

function List({ setBasketProducts, setFavProducts, favProducts, data }) {
  return (
    <ul className="list">
      {data.map(({ vendor_code, ...item }) => (
        <Card
          card={{ ...item, vendor_code }}
          key={vendor_code}
          setBasketProducts={setBasketProducts}
          setFavProducts={setFavProducts}
          favProducts={favProducts}
        />
      ))}
    </ul>
  );
}

export default List;
