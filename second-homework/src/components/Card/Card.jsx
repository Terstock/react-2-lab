import Button from "../Button/Button";
import "../Card/Card.scss";
import "../Button/Button.scss";
import "../Modal/Modal.scss";
import Modal from "../Modal/Modal";
import ModalWrapper from "../Modal/ModalWrapper";
import ModalHeader from "../Modal/ModalHeader";
import ModalClose from "../Modal/ModalClose";
import ModalBody from "../Modal/ModalBody";
import ModalFooter from "../Modal/ModalFooter";
import ModalImage from "../Modal/ModalImage";
import ModalText from "../Modal/ModalText";
import falseFavorite from "../assets/favorite-false.svg";
import trueFavorite from "../assets/favorite-true.svg";
import { useState, useEffect } from "react";
import PropTypes from "prop-types";
import "./Card.scss";

function Card({ setBasketProducts, setFavProducts, favProducts, card }) {
  const [isVisible, setIsVisible] = useState(false);
  const [modalType, setModalType] = useState(null);
  const isFavoritesAlready = favProducts.some(
    (product) => product.vendor_code === card.vendor_code
  );
  const [isFavorite, setIsFavorite] = useState(false);

  function changeModalState(type) {
    setIsVisible(!isVisible);
    setModalType(type);
  }

  function handleWrapperClick(e) {
    e.stopPropagation();
  }

  function toLocalStorage(card) {
    setBasketProducts((prev) => {
      const isInBasket = prev.some(
        (item) => item.vendor_code === card.vendor_code
      );
      if (isInBasket) {
        return [...prev];
      } else {
        return [...prev, card];
      }
    });
  }

  function handleFavorite(card) {
    setIsFavorite(!isFavorite);
    setFavProducts((prev) => {
      if (isFavorite) {
        return prev.filter((item) => item.vendor_code !== card.vendor_code);
      } else {
        return [...prev, card];
      }
    });
  }

  //Scroll remove
  useEffect(() => {
    if (isVisible === true) {
      document.body.classList.add("no-scroll");
    } else {
      document.body.classList.remove("no-scroll");
    }
  }, [isVisible]);

  return (
    <>
      <li className="card">
        <img className="card__photo" src={card.path} alt="" />
        <button
          className="card__button"
          onClick={() => {
            handleFavorite(card);
          }}
        >
          <img
            className="card__button-img"
            src={isFavoritesAlready ? trueFavorite : falseFavorite}
            alt=""
          />
        </button>
        <div className="card__info">
          <h1 className="card__title">{card.name}</h1>
          <h2 className="card__price">${card.price}</h2>
          <h3 className="card__color">Game disk color: {card.color}</h3>
          <h3 className="card__vendor-code">№{card.vendor_code}</h3>

          <div className="app-div">
            <Button
              type="button"
              onClick={() => changeModalState("add")}
              className="new__button new__button--white"
            >
              Buy Product
            </Button>
          </div>
        </div>
      </li>
      {isVisible ? (
        <ModalWrapper className="modal__wrapper" onClick={changeModalState}>
          <Modal className="modal" onClick={handleWrapperClick}>
            <ModalHeader className="modal__header">
              <ModalClose
                className="modal__close-btn"
                onClick={changeModalState}
              ></ModalClose>
            </ModalHeader>
            <ModalBody className="modal__body">
              <ModalText className="modal__body-title">
                Add Product {card.name}
              </ModalText>
              <ModalText className="modal__body-text">
                Description for you product
              </ModalText>
            </ModalBody>

            <ModalFooter
              firstText="ADD TO BASKET"
              firstClick={() => {
                toLocalStorage(card), changeModalState(false);
              }}
            ></ModalFooter>
          </Modal>
        </ModalWrapper>
      ) : null}
    </>
  );
}

Card.propTypes = {
  card: PropTypes.shape({
    name: PropTypes.string.isRequired,
    price: PropTypes.number.isRequired,
    path: PropTypes.string.isRequired,
    vendor_code: PropTypes.number.isRequired,
    color: PropTypes.string.isRequired,
  }).isRequired,
};

export default Card;
