import "./Modal.scss";

const ModalFooter = ({ firstText, firstClick }) => {
  return (
    <>
      {firstClick && firstText ? (
        <div className="modal__footer">
          <button
            type="submit"
            className="new__button new__button--purple"
            onClick={firstClick}
          >
            {firstText}
          </button>
        </div>
      ) : null}
    </>
  );
};

export default ModalFooter;
