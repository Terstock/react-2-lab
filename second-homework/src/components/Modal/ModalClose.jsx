import "./Modal.scss";
import closeIcon from "../assets/close-cross.svg";

function ModalClose({ className, onClick }) {
  return (
    <button onClick={onClick} className={className}>
      <img className="modal__close-icon" src={closeIcon} alt="" />
    </button>
  );
}

export default ModalClose;
